import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
// import logo from './logo.svg';
import './App.css';
import Weather from './pages/Weather';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
      <header />
      <Switch>
        <Route exact path='/' >
          <Weather />
          </Route>
      </Switch>
    {/* <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
