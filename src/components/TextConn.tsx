
// 미완성
import axios from 'axios';

const urlPath:string = 'https://api.openweathermap.org/data/2.5/weather';
const baseParam:string = '?units=metric&appid=c68966b120b82ed47df2a97fd66d0f36';
const url:string = urlPath + baseParam;

export const textConn = axios.create({
  baseURL: url,
  headers: {
    'Content-Type': 'multipart/form-data',
    Accept: 'application/json'
  },
  withCredentials: false,
  responseType: 'json',
  validateStatus: status => status >= 200 && status < 500
});