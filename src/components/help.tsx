import React from 'react';

/**
 * isEmpty
 * @param {mixed} input
 * @return {bool}
 */
 const isEmpty = (input:any) => !input || Object.keys(input).length === 0;
 
/**
 * getStorage
 * @param {*} storage
 * @param {*} key
 */
const getStorage = (storage:any, key:string) => {
  if (isEmpty(key)) {
    return null;
  }
  const returnValue = storage.getItem(key);

  if (isEmpty(returnValue)) {
    return null;
  }

  const { data, expire } = JSON.parse(returnValue);

  if (expire !== 0 && expire < Date.now()) {
    storage.removeItem(key);
    return null;
  }

  return data;
};

/**
 * setStorage
 * @param {*} storage
 * @param {*} key
 * @param {*} value
 * @param {number} expireSeconds (0 = 만료 체크를 하지 않음)
 */
const setStorage = (storage:any, key:string, value:string, expireSeconds = 0) => {
  if (isEmpty(key) || isEmpty(value)) {
    return false;
  }

  const data = {
    data: value,
    expire: expireSeconds === 0 ? 0 : Date.now() + expireSeconds * 1000
  };

  storage.setItem(key, JSON.stringify(data));
  return true;
};


/**
 * remove localstorage
 * @param {string} key
 * @return {bool}
 */
 export const removeStorage = (key:string) => {
  if (isEmpty(key)) {
    return false;
  }
  localStorage.removeItem(key);
  return true;
};

/**
 * get localstorage
 * @param {string} key
 * @return {string || any}
 */
 export const getLocalStorage = (key:string) => {
  return getStorage(localStorage, key);
};

/**
 * set localstorage
 * @param {string} key
 * @param {string || any} value
 * @param {number} expireSeconds (0 = 만료 체크를 하지 않음)
 * @return {bool}
 */
 export const setLocalStorage = (key:string, value:any, expireSeconds = 0) => {
  return setStorage(localStorage, key, value, expireSeconds);
};

