export interface BaseCoord {
  id: number;
  name: string;
  lon: number;
  lat: number;
}

export const BASE_COORD_LIST: BaseCoord[] = [
  { id: 1, name: '서울', lon: 126.98, lat: 37.563 },
  { id: 2, name: '부산', lon: 129.076, lat: 35.177 },
  { id: 3, name: '대구', lon: 128.608, lat: 35.867 },
  { id: 4, name: '인천', lon: 126.707, lat: 37.453 },
  { id: 5, name: '광주', lon: 126.853, lat: 35.156 },
  { id: 6, name: '대전', lon: 127.386, lat: 36.347 },
  { id: 7, name: '울산', lon: 129.313, lat: 35.535 },
  { id: 8, name: '세종', lon: 127.289, lat: 36.48 },
  { id: 9, name: '강릉', lon: 128.878, lat: 37.749 },
  { id: 10, name: '독도', lon: 131.864, lat: 37.241 },
  { id: 11, name: '울릉도', lon: 130.903, lat: 37.48 },
  { id: 12, name: '제주도', lon: 126.5, lat: 33.485 },
];