import React, {useState, useEffect, useCallback} from 'react';
// import {Link} from 'react-router-dom';
import axios from 'axios';
import '../index.css';
import {BASE_COORD_LIST,BaseCoord} from '../components/BaseData';
import {ApiResponseType} from '../components/Api'
import { setLocalStorage, getLocalStorage } from '../components/help';

interface StorageData {
  storage:any;
  key:string  
}

/**
 * 지역 리스트는 hard coding -> 지역이름, 좌표값
 * 페이지 랜딩시 현 시간, 특정 지역 날씨 정보 보여주기
 * 랜딩페이지 -> 지역 리스트 클릭 했을 때 날씨 정보 갱신
 * 지역 추가 -> 각자의 방식으로 저장
 * https://api.openweathermap.org/data/2.5/weather?units=metric&appid=c68966b120b82ed47df2a97fd66d0f36&q=London
 * 지역 삭제
 **/
export default function Waether (){
  const urlPath:string = 'https://api.openweathermap.org/data/2.5/weather';
  const baseParam:string = '?units=metric&appid=c68966b120b82ed47df2a97fd66d0f36';

  const [data, setData] = useState<any>([]);
  const [main, setMain] = useState<ApiResponseType>();
  const url:string = urlPath + baseParam;

  const textConn = axios.create({
    baseURL: url,
    headers: {
      'Content-Type': 'multipart/form-data',
      Accept: 'application/text'
    },
    withCredentials: false,
    responseType: 'json',
    validateStatus: status => status >= 200 && status < 500
  });

  
  const handleOpenWeather = (event: React.MouseEvent<HTMLAnchorElement> | undefined) => {
    if(event){
      event.preventDefault();
      // console.log('event?.currentTarget.dataset ==>>',event.currentTarget.dataset.item);
      // openWeatherCall(event.currentTarget.dataset.item);
      // console.log('event ==>>',event);
      // const { item:BaseCoord } = event?.currentTarget.dataset;
      // const item = event;
      // openWeatherCall(event.currentTarget.dataset.item);
    }
  };

  const openWeatherCall = useCallback(async (data:BaseCoord) => {
    if(data){
      const openWeatherUrl:string = url + '&lon=' +data.lon+ '&lat='+data.lat;
      const res = await textConn.get(openWeatherUrl);
      if(res){
        setMain(res.data);
      }else{
        alert('없습니다.');
      }
    }
  }, []);

  const openWeatherDelete = useCallback(async (item:BaseCoord) => {
    const localData = getLocalStorage('data');
    localData.splice((item.id)-1,1);
    setData(localData);
    setLocalStorage('data', localData);
    // })
  }, []);

  // 데이터 로드
  useEffect(() => {
    if(data?.length > 0){
      setData(getLocalStorage('data'));
    }else{
      const baseCoordList = BASE_COORD_LIST;
      setLocalStorage('data', baseCoordList);
      setData(baseCoordList);
    }
  }, []);
  
  // 초기셋팅
  useEffect(() => {
    !main &&  openWeatherCall(data[0]);
  }, []);

  // 확인
  useEffect(() => {
    console.log('data ==>>',data);
  }, []);

  return (
  <div className='wrap'>
    {main && (
      <div className='main'>
        <h2>날씨 메인 정보</h2>
        <div className='left'>
          <ul>
            <li data-title='실행시간'>{main.dt}</li>
            <li data-title='도시명(국가약어)'>{main.name} ({main.sys.country})</li>
            <li data-title='날씨아이콘'>{main.weather[0].icon}</li>
            <li data-title='경도/위도'>
              <p data-title='경도'>{main.coord.lon}</p>
              <p data-title='위도'>{main.coord.lat}</p>
            </li>
            <li data-title='날씨코드/날씨상세'>
              <p data-title='날씨코드'>{main.weather[0].main}</p>
              <p data-title='날씨상세'>{main.weather[0].description}</p>
            </li>
            <li data-title='온도/구름흐림도'>
              <p data-title='온도'>{main.main.temp}</p>
              <p data-title='구름흐림도'>{main.clouds.all}</p>
            </li>
            <li data-title='습도/풍속'>
              <p data-title='습도'>{main.main.humidity}</p>
              <p data-title='풍속'>{main.wind.speed}</p>
            </li>
          </ul>
        </div>
      </div>
    )}
    <div className='list'>
      <p className='input'>
        <input type='text'/>
        <input type='button' value='등록' />
      </p>
      <p>다른지역 리스트</p>
      <ul className='map'>
      {data && data.map((item:BaseCoord, index:number) => (
        <li key={index}>
        {/* <a onClick={handleOpenWeather} data-item={item}> */}
          <a onClick={()=>openWeatherCall(item)}>
          <dl>
            <dt data-title='지역명'>{item.name}</dt>
            <dd data-title='경도'>{item.lon}</dd>
            <dd data-title='위도'>{item.lat}</dd>
          </dl>
          </a>
          <button onClick={()=>openWeatherDelete(item)}>삭제</button>
        </li>
      ))}
      </ul>
    </div>
  </div>);
}


// 보관

  
// useEffect(() => {
//   // let listTemp:ApiResponseType[] = [];
//   // const fetch = async (BaseCoord:BaseCoord) => {
//   //     const openWeatherUrl:string = url + '&lon=' +BaseCoord.lon+ '&lat='+BaseCoord.lat;
//   //     const res = await textConn.get(openWeatherUrl);
//   //     if(res){
//   //       listTemp = [...listTemp, res.data];
//   //       setList(listTemp);
//   //     }
//   //   }
    
//   openWeatherCall(BASE_COORD_LIST[0]);
//   // BASE_COORD_LIST.forEach(item => {
//   //   fetch(item);
//   // });
// }, []);

